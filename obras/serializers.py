from rest_framework import serializers
from .models import Obra, Autor
from rest_framework.exceptions import ValidationError


class AutorSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Autor
        fields = "__all__" # Todos os campos da model
        
        extra_kwargs = {
            "id": {"read_only": True},
            "name": {"validators": []}
        }


class ObraSerializer(serializers.ModelSerializer):
    autores = AutorSerializer(many=True)

    
    class Meta:
        model = Obra
        fields = "__all__"
        
        extra_kwargs = {
            "id": {"read_only": True},
            # "title": {"validators": []},
            "criado": {"read_only": True}
        }
        
        depth = 1


    def create(self, validated_data):
        autores = validated_data.pop("autores")
        autores_list = []

        if not autores:
            raise ValidationError({"autores": ["At least one autor must be specified"]})

        obra_instance = Obra.objects.filter(titulo=validated_data["titulo"])

        if Obra.objects.filter(titulo=validated_data["titulo"]).exists():
            return obra_instance[0]
 
        obra_instance = super().create(validated_data)

        for autor in autores:

            autor = Autor.objects.get_or_create(**autor)[0]
            autores_list.append(autor)

        obra_instance.autores.set(autores_list)

        return obra_instance


    def update(self, instance, validated_data):
        autores = validated_data.pop("autores")
        autores_list = []
        for attr, value in validated_data.items():
                setattr(instance, attr, value)

        instance.save()
        
        for autor in autores:

            autor = Autor.objects.get_or_create(**autor)[0]
            autores_list.append(autor)

        instance.autores.set(autores_list)

        return instance