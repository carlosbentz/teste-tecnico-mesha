from rest_framework.routers import DefaultRouter
from .views import ObraModelViewSet


router = DefaultRouter()
router.register(r'obras', ObraModelViewSet)
urlpatterns = router.urls
