from .serializers import ObraSerializer
from .models import Obra
from rest_framework import viewsets
import csv
from django.http import HttpResponse
from rest_framework.views import APIView


class ObraModelViewSet(viewsets.ModelViewSet):
    queryset = Obra.objects.all()
    serializer_class = ObraSerializer
    lookup_field = "id"
