from django.db import models


class Autor(models.Model):
    nome = models.CharField(max_length=255)


class Obra(models.Model):
    titulo = models.CharField(max_length=255, unique=True)
    editora = models.CharField(max_length=255)
    foto = models.TextField()
    autores = models.ManyToManyField(Autor, related_name="obras")
    criado = models.DateTimeField(auto_now_add=True)