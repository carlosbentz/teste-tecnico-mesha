import csv
from django.http import HttpResponse
from rest_framework.views import APIView
from obras.models import Obra 
from obras.serializers import ObraSerializer
from rest_framework.response import Response
from rest_framework import status
import io
import ast


class GetCsvView(APIView):

    def get(self, request):
        fieldnames = ["titulo", "editora", "autores", "foto", "criado"]
        ordem = request.query_params.get("ordem")

        obras = Obra.objects.all()

        if ordem == "crescente":
            obras = obras.order_by("criado")

        if ordem == "decrescente":
            obras = obras.order_by("criado").reverse()

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="export.csv"'

        writer = csv.DictWriter(response, fieldnames=fieldnames)
        writer.writeheader()

        for obra in obras:
            autores_list = obra.autores.all()
            autores = []

            for autor in autores_list:
                autores.append({"nome": autor.nome})

            writer.writerow({
                    "titulo": obra.titulo, 
                    "editora": obra.editora,
                    "autores": str(autores), 
                    "foto": obra.foto,
                    "criado": obra.criado,
                })

        return response


class UploadCsvView(APIView):

    def post(self, request):
        fieldnames = ["titulo", "editora", "autores", "foto"]

        with io.TextIOWrapper(request.FILES["csv"], encoding='utf-8') as text_file:
            reader = csv.DictReader(text_file, delimiter=',')
            if not reader.fieldnames == fieldnames:
                return Response(data={"error": "Invalid csv fieldnames"}, status=status.HTTP_400_BAD_REQUEST)

            for row in reader:
                row["autores"] = ast.literal_eval(row["autores"])
                test = ObraSerializer(data=row)

                if test.is_valid():
                    test.save()

        return Response(status=status.HTTP_201_CREATED)
