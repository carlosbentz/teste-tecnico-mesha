from django.apps import AppConfig


class ObrasCsvConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'obras_csv'
