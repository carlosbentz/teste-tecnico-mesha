from django.urls import path
from .views import GetCsvView, UploadCsvView

urlpatterns = [
    path('file-obras/', GetCsvView.as_view()),
    path('upload-obras/', UploadCsvView.as_view()),
]
