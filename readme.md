# Teste técnico Mesha

# Este passo é para baixar o projeto

`git clone https://gitlab.com/carlosbentz/teste-tecnico-mesha`

## Entrar na pasta

`cd teste-tecnico-mesha`

## Criar um ambiente virtual

`python3 -m venv venv`

## Entrar no ambiente virtual

`source venv/bin/activate`

## Instalar as dependências

`pip install -r requirements.txt`

## Criar o banco de dados

`./manage.py migrate`

## Rodar localmente

`./manage.py runserver`
Por padrão, irá rodar em `http://127.0.0.1:8000/`

# Rotas

## Obras:

### `POST /obras`

```json
{
  "titulo": "No reino de gelo",
  "editora": "Intrínseca",
  "autores": [{ "nome": "Hampton Sides" }, { "nome": "Segundo Autor..." }],
  "foto": "https://picsum.photos/200"
}
```

```json
//  RESPONSE  STATUS  ->  HTTP  201  CREATED

{
  "id": 1,
  "titulo": "No reino de gelo",
  "editora": "Intrínseca",
  "autores": [
    { "id": 1, "nome": "Hampton Sides" },
    { "id": 2, "nome": "Segundo Autor..." }
  ],
  "foto": "https://picsum.photos/200",
  "criado_em": "12/10/2021"
}
```

Não deve ser possível realizar a criação de duas obras com o mesmo nome, caso isso aconteça, a aplicação deverá retornar a obra que está cadastrado no sistema.
Caso seja informado um autor já existente, será associado a obra mas mantendo seu id original.

### `GET /obras/`

```json
//  RESPONSE  STATUS  ->  200  OK
[
  {
    "id": 1,
    "titulo": "No reino de gelo",
    "editora": "Intrínseca",
    "autores": [
      { "id": 1, "nome": "Hampton Sides" },
      { "id": 2, "nome": "Segundo Autor..." }
    ],
    "foto": "https://picsum.photos/200",
    "criado_em": "12/10/2021"
  },
  {
    "id": 2,
    "titulo": "Ghost Soldiers",
    "editora": "Anchor",
    "autores": [{ "id": 1, "nome": "Hampton Sides" }],
    "foto": "https://picsum.photos/200",
    "criado_em": "12/10/2021"
  }
]
```

### `GET /obras/<int:obra_id>/`

```json
//  RESPONSE  STATUS  ->  HTTP  200  OK
{
  "id": 2,
  "titulo": "Ghost Soldiers",
  "editora": "Anchor",
  "autores": [{ "id": 1, "nome": "Hampton Sides" }],
  "foto": "https://picsum.photos/200",
  "criado_em": "12/10/2021"
}
```

Caso seja passado um id inválido, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
//  RESPONSE  STATUS  ->  HTTP  404  NOT  FOUND
{
  "detail": "Not found."
}
```

### `PUT /obras/<int:obra_id>/`

```json
{
  "titulo": "The Walking Dead: Declínio",
  "editora": "Galera",
  "autores": [{ "nome": "Robert Kirkman" }, { "nome": "Jay Bonansinga" }],
  "foto": "https://m.media-amazon.com/images/I/51vhXhK9dDL.jpg"
}
```

```json
//  RESPONSE  STATUS  ->  HTTP  200  OK
{
  "id": 1,
  "titulo": "The Walking Dead: Declínio",
  "editora": "Galera",
  "autores": [
    { "id": 3, "nome": "Robert Kirkman" },
    { "id": 4, "nome": "Jay Bonansinga" }
  ],
  "foto": "https://m.media-amazon.com/images/I/51vhXhK9dDL.jpg",
  "criado_em": "12/10/2021"
}
```

Caso seja passado um id inválido, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
//  RESPONSE  STATUS  ->  HTTP  404  NOT  FOUND
{
  "detail": "Not found."
}
```

### `DELETE /obras/<int:obra_id>/`

```json
//  REQUEST
```

```json
//  RESPONSE  STATUS  ->  HTTP  204
```

Caso seja passado um movie_id inválido, deverá retornar um erro `HTTP 404 - NOT FOUND`.

```json
//  RESPONSE  STATUS  ->  HTTP  404  NOT  FOUND
{
  "detail": "Not found."
}
```

## Obras - CSV:

### `POST /upload-obras/`

Esta rota receberá um arquivo csv contendo os mesmos parâmetros da rota "post /obras/" mas podendo ser salvo em massa no banco de dados.
Atenção: O Arquivo deverá vir com os fieldnames corretos, do contrário, retornará um status `HTTP 400 - BAD REQUEST`.
Caso alguma linha incorreta, a operação não será abortada, ela apenas será ignorada, continuando para a linha seguinte.

Fieldnames: `titulo, editora, autores, foto`

```json

    titulo,editora,autores,foto
    No Reino de gelo,Intrínseca,[{"nome": "Hampton Sides}],picsum.photos/
	The Walking Dead: Declínio,Galera,[{"nome":  "Robert Kirkman"},{"nome":  "Jay Bonansinga"}],https://m.media-amazon.com/images/I/51vhXhK9dDL.jpg

```

Exemplo de requisição:

![title](Images/mesha1.png)
![title](Images/mesha2.png)

```json
//  RESPONSE  STATUS  ->  HTTP  201  CREATED
[
  {
    "id": 1,
    "titulo": "No reino de gelo",
    "editora": "Intrínseca",
    "autores": [{ "id": 1, "nome": "Hampton Sides" }],
    "foto": "picsum.photos/",
    "criado_em": "12/10/2021"
  },
  {
    "id": 2,
    "titulo": "The Walking Dead: Declínio",
    "editora": "Galera",
    "autores": [
      { "id": 2, "nome": "Robert Kirkman" },
      { "id": 3, "nome": "Jay Bonansinga" }
    ],
    "foto": "https://m.media-amazon.com/images/I/51vhXhK9dDL.jpg",
    "criado_em": "12/10/2021"
  }
]
```

### `GET /file-obras/`

Esta rota retornará um arquivo csv contendo todas as obras registradas no banco de dados.
É possível filtrar a ordem de retorno pela data de criação, exemplo:
`GET /file-obras?ordem=crescente` ou `GET /file-obras?ordem=decrescente`

```json
//  RESPONSE  STATUS  ->  200  OK

	titulo,editora,autores,foto,criado_em
    No Reino de gelo,Intrínseca,[{"nome": "Hampton Sides}],picsum.photos/,12/10/2021
	The Walking Dead: Declínio,Galera,[{"nome":  "Robert Kirkman"},{"nome":  "Jay Bonansinga"}],https://m.media-amazon.com/images/I/51vhXhK9dDL.jpg,12/10/2021
```

## Tecnologias utilizadas 📱

- Django
- Django Rest Framework
- Sqlite
- Coverage
- stackedit.io (Para documentação)
- dbdiagram.io (Diagrama)
